#!/usr/bin/python3
import sys, getopt


inputfile = ''
outputfile = ''



def permutator(origen,word,f,fila,cadenes):

	permutacions = [["a","4"],["a","@"],["e","3"],["A","4"],["A","@"],["E","3"],["i","1"],["I","1"],["o","0"],["O","0"],["o","@"],["O","@"],["s","$"],["S","$"],["s","5"],["S",5],["i","!"],["I","!"],["l","1"],["L","1"],["u","1"],["U","1"]]
	if word not in cadenes:
		cadenes.append(word)
		f.write(word+"\n") 
#	while fila in permutacions:
#		if fila[0] in word:		
#			word = word.replace(fila[0],fila[1])
#			f.write(word+"\n") 	
	while fila < len(permutacions):
		if permutacions[fila][0] in word:
			permutator(word,word,f,fila+1,cadenes)
#			word = word.replace(fila[0],fila[1])
#			f.write(word.replace(fila[0],fila[1])+"\n")
			permutator(word,word.replace(permutacions[fila][0],permutacions[fila][1]),f,fila+1,cadenes)			 	
		fila += 1						


def filereader(inputfile,outputfile):
	f = open(outputfile, 'w')
	
	myfile = open(inputfile, "r+")

	myline = myfile.readline()
	while myline:	    
		
		for word in myline.split():
			if len(str(word)) != 0 :
			    variable = word
			    cadenes = []
			    permutator("",word,f,0,cadenes)
		myline = myfile.readline()
	myfile.close()
	f.close()   	



def main(argv):


   print ("Pass utilities: Smart Dictionary Permutator")
# Comprovar params
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print ('dictPermutator.py -i <inputfile> -o <outputfile>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print ('dictPermutator.py -i <inputfile> -o <outputfile>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg

# Statistics
   lineso = 0
   linesin = 0
   with open(inputfile) as lector:
      lineso = len(lector.readlines())
      print("Original file have ",lineso," lines")

   filereader(inputfile,outputfile) 
   with open(outputfile) as lector:  
      linesin = len(lector.readlines())
   lines = linesin - lineso
   print("Generated passwords: ",lines)

if __name__ == "__main__":
   main(sys.argv[1:])
