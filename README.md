
# Passutilites

## Description
- Utilities for working with passwords in an Ethical Hacking way


## passUtilities.py

- All utilities in single line or in menu

## dictPermutator.py:

- Permutate a dictionary trying to keep typical transformations thar are done at generating passwords
- Generate multiple variations of words changing some letters that are typically used to bypass password policies. a/A->4/@, e/E->3, i/I->1/!, o/O->0/@, s/S->$/5, l/L->1, u,U->1

## filter.py

- Filter words in list that match some password policies
- Valid filters are
	- Minimum length
	- Maximum length
	- Include lowercase
	- Include uppercas
	- Include digits
	- Include specialchars

## capitalizer.py

- Generate capitalized and non capitalized words

## Installation

- Simply clone this repo:

```
git clone https://gitlab.com/roger.sanchez/passutilities.git
```

## Usage

## Common parameters

- "-i" or "ifile=file" : input file / original dictionary file
- "-o" or "ofile=file" : output file / generated dictionary file

### Generate Smart Dictionary from existing one


```
python3 permutator.py  -o out.txt -i rockreduced.txt

```


## Support

- Not yet

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

### filter

- Filter words in list that match some password policies
- Valid filters are
	- Minimum length
	- Maximum length
	- Include lowercase
	- Include uppercas
	- Include digits
	- Include specialchars



## Contributing

- Not yet

## Authors and acknowledgment

- Not yet

## License

- This software is licensed by GNU General Public License ![GPL](imgs/gplv3-127x51.png)

## Project status

- Just started

[<img src="https://i.ytimg.com/vi/Hc79sDi3f0U/maxresdefault.jpg" width="50%">](https://www.youtube.com/watch?v=Hc79sDi3f0U "Now in Android: 55")     
